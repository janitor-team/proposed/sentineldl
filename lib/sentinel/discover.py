#!/usr/bin/env python3

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pickle

from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt


def discover(
        username, password, footprint, platformname, producttype, dates,
        clouds, debug_save=False):

    sentinel_api = SentinelAPI(username, password)

    footprint = geojson_to_wkt(read_geojson( footprint ))

    products = sentinel_api.query(
            footprint,
            platformname = platformname,
            producttype = producttype,
            date = dates,
            cloudcoverpercentage = clouds
            )
    products_df = sentinel_api.to_dataframe(products)

    if debug_save == True:
        with open('products_df.pickle', 'wb') as f:
            pickle.dump(products_df, f)
            f.truncate()

    return(products_df)
