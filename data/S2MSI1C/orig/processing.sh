#!/bin/bash

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

BASE_DIRECTORY="$(dirname ${PWD})"
export BASE_DIRECTORY

cd "${BASE_DIRECTORY}"

Runscripts()
{
	ZIP="${1}"

	for SCRIPT in orig/processing.d/????-*
	do
		if [ -x "${SCRIPT}" ]
		then
			echo "Running ${SCRIPT}"
			./${SCRIPT} "${ZIP}"
		else
			echo "Skipping ${SCRIPT}"
		fi
	done
}

export -f Runscripts

ZIPS="$(find orig/????? -name "*.zip")"

parallel --bar Runscripts ::: ${ZIPS}
