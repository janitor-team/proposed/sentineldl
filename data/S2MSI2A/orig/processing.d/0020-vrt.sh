#!/bin/sh

# Copyright (C) 2018-2020 Simon Spöhel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

ZIP="${1}"

DIRECTORY="$(dirname ${ZIP} | cut -d/ -f 2-)"
FILE="$(basename ${ZIP} .zip)"

cd "${BASE_DIRECTORY}"

if [ ! -f "VRT/${DIRECTORY}/${FILE}.vrt.sha512" ]
then
	echo -n "Creating virtual layer stack (vrt) for ${FILE} "

	mkdir -p "VRT/${DIRECTORY}"
	cd "VRT/${DIRECTORY}"

	rm -f "${FILE}.vrt"

	gdalbuildvrt											\
		-separate										\
		-resolution highest									\
		-srcnodata 0										\
		${FILE}.vrt										\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R60m/*_B01_60m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R10m/*_B0[2-4]_10m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R20m/*_B0[5-7]_20m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R10m/*_B08_10m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R20m/*_B8A_20m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R60m/*_B09_60m.jp2	\
		../../../SAFE/${DIRECTORY}/${FILE}.SAFE/GRANULE/L2A*/IMG_DATA/R20m/*_B1[1-2]_20m.jp2

	sha512sum "${FILE}.vrt" > "${FILE}.vrt.sha512"
fi

cd "${OLDPWD}"

if [ ! -e "VRT/${DIRECTORY}/${FILE}.qml" ]
then
	echo -n "Creating QGIS layer style file (qml) for ${FILE} "

cat > "VRT/${DIRECTORY}/${FILE}.qml" << EOF
<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.18.9" minimumScale="inf" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" blueBand="2" greenBand="3" type="multibandcolor" redBand="4">
      <rasterTransparency/>
      <redContrastEnhancement>
        <minValue>0</minValue>
        <maxValue>2000</maxValue>
        <algorithm>StretchToMinimumMaximum</algorithm>
      </redContrastEnhancement>
      <greenContrastEnhancement>
        <minValue>0</minValue>
        <maxValue>2000</maxValue>
        <algorithm>StretchToMinimumMaximum</algorithm>
      </greenContrastEnhancement>
      <blueContrastEnhancement>
        <minValue>0</minValue>
        <maxValue>2000</maxValue>
        <algorithm>StretchToMinimumMaximum</algorithm>
      </blueContrastEnhancement>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
EOF

	echo " done."
fi
